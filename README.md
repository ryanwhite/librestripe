# README #

Note: switched to gitlab for hosting.

To get running, just download the repository, and open the "form.html" file in your browser. Fill in test credit card details available from stripe: [https://stripe.com/docs/testing](https://stripe.com/docs/testing) and you should be walked through the process of getting a token for the purposes of doing a transaction.

This code is not related to the server side code you would need to run (with your private stripe account details) to actually perform the deduction.

### What is this code for? ###

* It aims to give you a LibreJS compliant way to use Stripe
* Pre-alpha proof of concept at this stage

For more on LibreJS - visit [http://www.gnu.org/software/librejs/](http://www.gnu.org/software/librejs/)

### How do I get set up? ###

* Summary of set up - just download the files, open form.html in your browser
* Configuration - none required, but you could be running the LibreJS add-on for Mozilla based browsers if you want to
* Dependencies - internet connection, account with Stripe, if you want to use your own publishable key
* Database configuration - none
* How to run tests - play with the form
* Deployment instructions - none

### Contribution guidelines ###

I'm open for suggestions and contributions on everything:

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* mail me at ryan.white+librestripe@systemiclogic.net for any and all questions etc. You can also contribute on the wiki and issue trackers
* Other community or team contact - could be you! - just send me a mail :)
